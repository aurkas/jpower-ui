module.exports = {
  lintOnSave: true,
  productionSourceMap: false,
  chainWebpack: (config) => {
    const entry = config.entry('app')
    entry
        .add('babel-polyfill')
        .end()
    entry
        .add('classlist-polyfill')
        .end()
    entry
        .add('@/mock')
        .end()
  },
  devServer: {
    // 端口配置
    port: 8082,
    // 反向代理配置
    proxy: {
      '/api': {
        target: 'http://localhost:80',
        ws: true,
        pathRewrite: {
          '^/api': '/'
        }
      }
    }
  }
}
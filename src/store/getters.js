
const getters = {
  tag: state => state.tags.tag,
  language: state => state.common.language,
  isUpdatePassword: state => state.common.isUpdatePassword,
  website: state => state.common.website,
  userInfo: state => state.user.userInfo,
  passFlag: state => state.user.passFlag,
  colorName: state => state.common.colorName,
  themeName: state => state.common.themeName,
  isShade: state => state.common.isShade,
  isCollapse: state => state.common.isCollapse,
  keyCollapse: (state, getters) => getters.screen > 1 ? getters.isCollapse : false,
  screen: state => state.common.screen,
  isLock: state => state.common.isLock,
  isFullScren: state => state.common.isFullScren,
  isMenu: state => state.common.isMenu,
  lockPasswd: state => state.common.lockPasswd,
  menuBtnPer: state => state.common.menuBtnPer,
  tagList: state => state.tags.tagList,
  tagWel: state => state.tags.tagWel,
  token: state => state.user.token,
  rtoken: state => state.user.rtoken,
  tokenType: state => state.user.tokenType,
  expiresIn: state => state.user.expiresIn,
  rolesId: state => state.user.rolesId,
  tenantCode: state => state.user.tenantCode,
  logo: state => state.user.logo,
  permission: state => state.user.permission,
  menu: state => state.user.menu,
  isShowTenantCode: (state, getters) => {
    if (!getters.website.isTenant){
      return false;
    }

    return state.user.rolesId.includes(getters.website.rootRoleId);
  },
  logsList: state => state.logs.logsList,
  logsLen: state => state.logs.logsList.length || 0,
  logsFlag: (state, getters) => getters.logsLen === 0
}
export default getters
import {
  setStore,
  getStore,
  removeStore
} from '@/util/store'
import website from '@/config/website'
const common = {
  state: {
    language: getStore({ name: 'language' }) || 'zh',
    isCollapse: false,
    isFullScren: false,
    isMenu: true,
    isShade: false,
    screen: -1,
    isLock: getStore({ name: 'isLock' }) || false,
    showTag: true,
    showDebug: true,
    showCollapse: true,
    showSearch: true,
    showLock: true,
    showFullScren: true,
    showTheme: true,
    showMenu: true,
    showColor: true,
    //默认颜色
    colorName: getStore({ name: 'colorName' }) || '#409eff',
    //设置默认主题
    themeName: getStore({name: 'themeName'}) || 'default',
    // themeName: getStore({name: 'themeName'}) || 'theme-beautiful',
    lockPasswd: getStore({ name: 'lockPasswd' }) || '',
    menuBtnPer: getStore({ name: 'menuBtnPer' }) || {},
    website: website,
    isUpdatePassword: false
  },
  mutations: {
    SET_LANGUAGE: (state, language) => {
      state.language = language
      setStore({
        name: 'language',
        content: state.language
      })
    },
    SET_ISUPDATEPASSWORD: (state, isUpdatePassword) => {
      state.isUpdatePassword = isUpdatePassword
      setStore({
        name: 'isUpdatePassword',
        content: state.isUpdatePassword
      })
    },
    SET_SHADE: (state, active) => {
      state.isShade = active;
    },
    SET_COLLAPSE: (state) => {
      state.isCollapse = !state.isCollapse;
    },
    SET_IS_MENU: (state, menu) => {
      state.isMenu = menu;
    },
    SET_FULLSCREN: (state) => {
      state.isFullScren = !state.isFullScren;
    },
    SET_LOCK: (state) => {
      state.isLock = true;
      setStore({
        name: 'isLock',
        content: state.isLock,
        type: 'session'
      })
    },
    SET_SCREEN: (state, screen) => {
      state.screen = screen;
    },
    SET_COLOR_NAME: (state, colorName) => {
      state.colorName = colorName;
      setStore({
        name: 'colorName',
        content: state.colorName,
      })
    },
    SET_THEME_NAME: (state, themeName) => {
      state.themeName = themeName;
      setStore({
        name: 'themeName',
        content: state.themeName,
      })
    },
    SET_LOCK_PASSWD: (state, lockPasswd) => {
      state.lockPasswd = lockPasswd;
      setStore({
        name: 'lockPasswd',
        content: state.lockPasswd,
        type: 'session'
      })
    },
    SET_MENU_BTN_PER: (state,menuBtnPer) => {
      state.menuBtnPer = menuBtnPer;
      setStore({
        name: 'menuBtnPer',
        content: state.menuBtnPer,
        type: 'session'
      })
    },
    CLEAR_LOCK: (state) => {
      state.isLock = false;
      state.lockPasswd = '';
      removeStore({
        name: 'lockPasswd',
        type: 'session'
      });
      removeStore({
        name: 'isLock',
        type: 'session'
      });
    },
    CLEAR_MENUBTNPER: (state) => {
      state.menuBtnPer = {};
      removeStore({
        name: 'menuBtnPer',
        type: 'session'
      });
    }
  }
}
export default common
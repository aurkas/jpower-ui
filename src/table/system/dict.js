
export default (safe)=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    selection: true,
    tip: false,
    border: true,
    searchMenuSpan: 6,
    lazy: true,
    viewBtn: true,
    rowKey: 'id',
    tree: true,
    labelWidth: 120,
    menuWidth: 300,
    addBtn: !safe.validatenull(safe.oldDictType.id),
    emptyText: safe.oldDictType.dictTypeCode?'暂无数据':'未选择字典类型',
    permission: {
      addCode: 'SYSTEM_DICT_SAVE',
      editCode: 'SYSTEM_DICT_SAVE',
      delCode: 'SYSTEM_DICT_DELETE',
      viewCode: 'SYSTEM_DICT_DETAIL',
      editTypeCode: 'SYSTEM_DICT_TYPE_UPDATE',
      delTypeCode: 'SYSTEM_DICT_TYPE_DELETE',
      addTypeCode: 'SYSTEM_DICT_TYPE_ADD'
    },
    align: "center",
    column: [
      {
        label: "字典类型编码",
        row: true,
        span: 24,
        align: "left",
        prop: "dictTypeCode",
        value: safe.oldDictType.dictTypeCode,
        disabled: true,
        rules: [{
          required: true,
          message: "请输入字典名称",
          trigger: "blur"
        }]
      },{
        label: "字典名称",
        prop: "name",
        search: true,
        rules: [{
          required: true,
          message: "请输入字典名称",
          trigger: "blur"
        }]
      },
      {
        label: "字典编码",
        prop: "code",
        slot: true,
        search: true,
        rules: [{
          required: true,
          message: "请输入字典编号",
          trigger: "blur"
        }]
      },
      {
        label: "上级字典",
        prop: "parentName",
        disabled: true,
        value: ''-1,
        display: safe.oldDictType.isTree === 1,
        hide: true,
        showColumn: false
      },
      {
        label: "排序",
        prop: "sortNum",
        type: 'number',
        value: 0,
        showColumn: false,
        hide: true,
        precision: 0
      },
      {
        label: "备注",
        prop: "note",
        type: "textarea",
        span: 24,
        hide: true,
        overHidden: true,
        minRows: 6,
      }],
  }
}
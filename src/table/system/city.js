
export const option = {
  submitText: '新 增',
  detail: true,
  emptyBtn: false,
  column: [
    {
      label: "父级地区",
      prop: "pname",
      disabled: true
    },
    {
      label: "编码",
      prop: "code",
      prepend: '',
      append: '0000000000',
      maxlength: 2,
      minlength: 2,
      blur: (data)=>{

        if (data.value){
          if (data.column.minlength === 2){
            if (data.value.length === 1){
              this.formData.code = '0'+this.formData.code;
            }
          } else if (data.column.minlength === 3){
            if (data.value.length === 1){
              this.formData.code = '00'+this.formData.code;
            } else if (data.value.length === 2){
              this.formData.code = '0'+this.formData.code;
            }
          }
        }
      },
      rules: [{
        required: true,
        message: "请输入编码",
        trigger: "blur"
      }]
    },
    {
      label: "名称",
      prop: "name",
      rules: [{
        required: true,
        message: "请输入名称",
        trigger: "blur"
      }]
    },
    {
      label: "全称",
      prop: "fullname",
      rules: [{
        required: true,
        message: "请输入全称",
        trigger: "blur"
      }]
    },
    {
      label: "经度",
      prop: "lng",
      type: 'number'
    },
    {
      label: "纬度",
      prop: "lat",
      type: 'number'
    },
    {
      label: "国家编码",
      prop: "countryCode",
      row: true,
      span: 24,
      value: 'CHN'
    },
    {
      label: "城市类型",
      prop: "cityType",
      row: true,
      span: 24,
      type: 'radio',
      border: true,
      dicUrl: window.urllist.dictUrl + 'CITY_TYPE',
      props: {
        label: "name",
        value: "code"
      },
      rules: [{
        required: true,
        message: "请输入城市类型",
        trigger: "blur"
      }]
    },
    {
      label: "排序",
      prop: "sortNum",
      type: 'number',
      precision: 0
    },
    {
      label: "地区级别",
      prop: "rankd",
      value: 1,
      type: 'select',
      dataType: 'number',
      dicUrl: window.urllist.dictUrl + 'CITY_LEVEL',
      disabled: true,
      props: {
        label: "name",
        value: "code"
      },
      rules: [{
        required: true,
        message: "请选择地区级别",
        trigger: "blur"
      }]
    },
    {
      label: "备注",
      prop: "note",
      type: 'textarea',
      span: 24
    }
  ]
}
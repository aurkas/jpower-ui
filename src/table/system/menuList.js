
import iconList from "@/config/iconList";
export default ()=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    lazy: true,
    tip: false,
    border: true,
    viewBtn: true,
    selection: true,
    menuWidth: 80,
    menuType: "menu",
    permission: {
      addCode: 'SYSTEM_FUNCTION_ADD',
      editCode: 'SYSTEM_FUNCTION_UPDATE',
      delCode: 'SYSTEM_FUNCTION_DELETE'
    },
    column: [{
      label: "名称",
      prop: "functionName",
      search: true,
      span: 12,
      rules: [{
        required: true,
        message: "请输入名称",
        trigger: "blur"
      }]
    },
      {
        label: "别名",
        prop: "alias",
        search: true,
        span: 12,
        hide: true,
        rules: [
          {
            required: true,
            message: "请输入别名",
            trigger: "blur"
          }
        ]

      },{
        label: '编号',
        prop: 'code',
        search: true,
        width: 140,
        overHidden: true,
        span: 12,
        rules: [
          {
            required: true,
            message: "请输入编号",
            trigger: "blur"
          }
        ]
      },{
        label: '打开方式',
        prop: 'target',
        span: 12,
        hide: true,
        slot: true,
        align: "center",
        type: 'select',
        value: '_self',
        dicUrl: window.urllist.dictUrl + 'DKFS',
        props: {
          label: "name",
          value: "code"
        },
        rules: [
          {
            required: true,
            message: "请选择页面打开方式",
            trigger: "blur"
          }
        ]
      }, {
        label: "接口地址",
        prop: "url",
        search: true,
        span: 24,
        overHidden: true,
        rules: [
          {
            required: false,
            message: "请输入接口地址",
            trigger: "change"
          }
        ]
      },
      {
        label: '是否菜单',
        prop: 'isMenu',
        type: "switch",
        width: 80,
        dataType: 'number',
        align: "center",
        search: true,
        span: 12,
        value: 0,
        dicUrl: window.urllist.dictUrl + 'YN01',
        props: {
          label: "name",
          value: "code"
        },
        rules: [
          {
            required: true,
            message: "请选择是否菜单",
            trigger: "blur"
          }
        ]
      },
      {
        label: "上级菜单",
        prop: "parentId",
        type: "tree",
        dataType: 'string',
        dicUrl: window.home_url + '/jpower-system/core/function/menuTree',
        showColumn: false,
        hide: true,
        span: 12,
        props: {
          label: "functionName",
          value: 'id',
        },
        rules: [
          {
            required: false,
            message: "请选择上级菜单",
            trigger: "click"
          }
        ]
      }, {
        label: "图标",
        prop: "icon",
        type: "icon",
        width: 80,
        align: "center",
        span: 12,
        iconList: iconList
      },{
        label: "排序",
        prop: "sort",
        span: 12,
        width: 80,
        type: "number",
        hide: true,
        showColumn: false
      },
      {
        label: "模块概述",
        prop: "moudeSummary",
        type: "textarea",
        span: 24,
        minRows: 6,
        hide: true
      },
      {
        label: "操作说明",
        prop: "operateInstruction",
        type: "textarea",
        span: 24,
        minRows: 6,
        hide: true
      }, {
        label: "备注",
        prop: "remark",
        type: "textarea",
        span: 24,
        minRows: 6,
        hide: true
      }],
  }
}
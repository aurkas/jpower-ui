export default ()=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    translate: false,
    searchLabelWidth: 100,
    menuWidth: 120,
    tip: false,
    align: 'center',
    headerAlign: 'center',
    border: true,
    stripe: true,
    viewBtn: true,
    addBtn: false,
    delBtn: false,
    editBtn: false,
    selection: true,
    dateBtn: true,
    permission: {
      excelCode: 'MONITOR_RESULTS_EXPORT'
    },
    column: [{
      label: "服务名称",
      prop: "name",
      search: true,
      width: 160
    },
    {
      label: "监控地址",
      prop: "path",
      search: true
    },

    {
      label: "接口地址",
      prop: "url",
      overHidden: true
    },

    {
      label: "请求方式",
      prop: "method",
      slot: true,
      width: 120
    },
    {
      label: "请求异常",
      prop: "error",
      showColumn: false,
      hide: true,
      minRows: 5,
      type: "textarea"
    },
    {
      label: "响应编码",
      prop: "resposeCode",
      width: 90
    },
    {
      label: "响应数据",
      prop: "respose",
      overHidden: true,
      minRows: 5,
      type: "textarea"
    },
    {
      label: "接口返回数据",
      prop: "restfulResponse",
      showColumn: false,
      hide: true,
      minRows: 5,
      type: "textarea"
    },
    {
      label: "header参数",
      prop: "header",
      showColumn: false,
      hide: true,
      minRows: 5,
      type: "textarea"
    },
    {
      label: "body参数",
      prop: "body",
      showColumn: false,
      hide: true,
      minRows: 5,
      type: "textarea"
    },
    {
      label: "是否成功",
      prop: "isSuccessStr",
      slot: true,
      width: 90
    },
    {
      label: "响应时长",
      prop: "responseTime",
      slot: true,
      append: 'ms',
      width: 90
    },
      {
        label: "监控时间",
        prop: "createTime",
        width: 160
      }],
  }
}
export default ()=> {
  return {
    index: true,
    indexLabel: '序号',
    indexFixed: false,
    tip: false,
    border: true,
    dateBtn: true,
    viewBtn: true,
    menuWidth: 80,
    expand: true,
    expandFixed: false,
    menuFixed: false,
    viewBtnText: '详情',
    column: [
      {
        label: "服务名称",
        prop: "serverName",
        search: true
      },
      {
        label: "服务器IP",
        prop: "serverIp"
      },
      {
        label: "运行环境",
        prop: "env",
        hide: true
      },
      {
        label: "客户端IP",
        prop: "operIp"
      },
      {
        label: "操作用户",
        prop: "operName",
        search: true
      },
      {
        label: "请求客户端",
        prop: "clientCode",
        search: true,
        searchLabelWidth: 100,
        searchslot: true
      },
      {
        label: "请求接口",
        prop: "url",
        overHidden: true
      },
      {
        label: "请求方式",
        prop: "method",
        hide: true
      },
      {
        label: "请求参数",
        prop: "param",
        type: 'textarea',
        minRows: 1,
        hide: true,
        span: 24,
        row: true
      },
      {
        label: "报错类名",
        prop: "methodClass",
        minRows: 1,
        span: 24,
        row: true,
        hide: true
      },
      {
        label: "报错方法名",
        prop: "methodName",
        hide: true
      },
      {
        label: "报错行号",
        prop: "lineNumber",
        hide: true
      },
      {
        label: "异常名称",
        prop: "exceptionName",
        search: true,
        slot: true,
        overHidden: true,
        width: 200,
        align: "center"
      },
      {
        label: "记录时间",
        prop: "createTime",
        slot: true,
        width: 180,
        align: "center"
      },
      {
        label: "message",
        type: "textarea",
        prop: "message",
        row: true,
        span: 24,
        minRows: 1,
        hide: true
      },
      {
        label: "错误信息",
        type: "textarea",
        prop: "error",
        row: true,
        span: 24,
        maxRows: 20,
        hide: true
      }
    ]
  }
}
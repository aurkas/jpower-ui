export default (safe)=> {
  //safe => vue的this对象
  return {
    index: true,
    indexLabel: '序号',
    tip: false,
    border: true,
    dateBtn: true,
    viewBtn: true,
    menuWidth: 80,
    viewBtnText: '详情',
    column: [
      {
        label: "标题",
        prop: "title",
        search: true,
        overHidden: true
      },
      {
        label: "业务类型",
        prop: "businessTypeStr",
        align: 'center',
        search: true,
        searchLabelWidth: 100,
        searchslot: true,
        slot: true
      },
      {
        label: "服务名称",
        prop: "serverName",
        search: true
      },
      {
        label: "服务器IP",
        prop: "serverIp",
        hide: true
      },
      {
        label: "运行环境",
        prop: "env",
        hide: true
      },
      {
        label: "客户端IP",
        prop: "operIp",
        hide: true
      },
      {
        label: "操作用户",
        prop: "operName",
        search: true
      },
      {
        label: "请求客户端",
        prop: "clientCode",
        search: true,
        searchLabelWidth: 100,
        searchslot: true
      },
      {
        label: "请求接口",
        prop: "url",
        overHidden: true
      },
      {
        label: "请求方式",
        prop: "method",
        hide: true
      },
      {
        label: "请求参数",
        prop: "param",
        type: 'textarea',
        minRows: 1,
        hide: true,
        span: 24,
        row: true
      },
      {
        label: "返回内容",
        prop: "returnContent",
        hide: true,
        span: 24,
        row: true,
        type: 'textarea',
        minRows: 1
      },
      {
        label: "请求类",
        prop: "methodClass",
        minRows: 1,
        span: 24,
        row: true,
        hide: true
      },
      {
        label: "请求方法",
        prop: "methodName",
        hide: true
      },
      {
        label: "操作状态",
        prop: "statusStr",
        align: 'center',
        slot: true
      },
      {
        label: "记录时间",
        prop: "createTime",
        align: 'center',
        slot: true
      },
      {
        label: "错误信息",
        prop: "errorMsg",
        hide: true,
        type: 'textarea',
        span: 24,
        row: true,
        display: safe.isDisplayShow,
        minRows: 1
      }

    ]
  }
}

import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.log


export const list = (data) => request.get(url + '/log/error/list', {
  params: data
})

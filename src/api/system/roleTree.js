
import request from '@/router/axios';
import { urllist } from '@/config/env';
import {validatenull} from "@/util/validate";
const url = urllist.system + '/core';
const user = urllist.user + '/core';

export const listByRole = (data) => request.get(user + '/user/listByRole', {
  params: data
})
export const addRoleUser = (data) => request({
  url: user + '/user/addRoleUser',
  method: 'put',
  meta: {
    isSerialize: true
  },
  data: data
});
export const listButByMenu = (data) => request.get(url + '/function/listButByMenu', {
  params: data
})
export const deleteRoleUser = (data) => request.delete(user + '/user/deleteRoleUser', {
  params: data
})
export const listIdByRoleId = (data) => request.get(url + '/dataScope/listIdByRoleId', {
  params: data
})
export const listByMenuId = (data) => request.get(url + '/dataScope/listByMenuId', {
  params: data
})
export const roleDataScope = (data) => request({
  url: url + '/dataScope/roleDataScope',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const queryUrlIdByRole = (data) => request.get(url + '/function/queryUrlIdByRole', {
  params: data
})
export const addFunction = (data) => request({
  url: url + '/role/addFunction',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const update = (id, data) => {

  if(data.hasOwnProperty("parentId") && validatenull(data.parentId)){
    data.parentId = "-1";
  }

  return request({
    url: url + '/role/update',
    method: 'put',
    meta: {
      isSerialize: true
    },
    data: data
  })
}
export const del = (id) => request.delete(url + '/role/deleteStatus', {
  params: {
    ids: id
  }
})
export const add = (data) => request({
  url: url + '/role/add',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const list = (data) => request.get(url + '/role/listTree', {
  params: data
})

import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.user + '/core';
const system = urllist.system + '/core';
export const getRoleList = (data) => {
  return request({
    url: system + '/role/listTree',
    method: 'get',
    params: data
  })
}
export const getOrgTree = (data) => {
  return request({
    url: system + '/org/tree',
    method: 'get',
    params: data
  })
}
export const list = (data) => {
  return request({
    url: url + '/user/list',
    method: 'get',
    params: data
  })
}
export const addRoles = (data) => {
  return request({
    url: url + '/user/addRole',
    method: 'post',
    meta: {
      isSerialize: true
    },
    data: data
  })
}
export const resetPassword = (ids) => {
  return request({
    url: url + '/user/resetPassword',
    method: 'put',
    meta: {
      isSerialize: true
    },
    data: { ids }
  })
}
export const del = (id) => request.delete(url + '/user/delete', {
  params: {
    ids: id
  }
})
export const add = (data) => request({
  url: url + '/user/add',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const update = (id, data) => request({
  url: url + '/user/update',
  method: 'put',
  meta: {
    isSerialize: true
  },
  data: data
})

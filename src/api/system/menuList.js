
import request from '@/router/axios';
import { urllist } from '@/config/env';
import {validatenull} from "@/util/validate";
const url = urllist.system

export const add = (data) => request({
  url: url + '/core/function/add',
  method: 'post',
  meta: {
    isSerialize: true
  },
  data: data
})
export const update = (id, data) => {

  if(data.hasOwnProperty("parentId") && validatenull(data.parentId)){
    data.parentId = "-1";
  }

  return request({
    url: url + '/core/function/update',
    method: 'put',
    meta: {
      isSerialize: true
    },
    data: data
  })
}
export const del = (id) => request.delete(url + '/core/function/delete', {
  params: {
    ids: id
  }
})
export const menuTree = () => request.get(url + '/core/function/menuTree');
export const list = (data = {}) => {

  if (data.hasOwnProperty('isMenu')){
    data['isMenu_eq'] = data.isMenu;
    delete data['isMenu'];
  }

  return request.get(url + '/core/function/listByParent', {
    params: data
  })
};


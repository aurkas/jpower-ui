
import request from '@/router/axios';
import { urllist } from '@/config/env';
const url = urllist.system

export const list = (data) => request.get(url + '/core/tenant/list', {
  params: data
})
export const setting = (data) => request({
  url: url + '/core/tenant/setting',
  method: 'put',
  meta: {
    isSerialize: true
  },
  data: data
});
export const del = (id) => request.delete(url + '/core/tenant/delete', {
  params: {
    ids:id
  }
})
export const add = (data) => {
  return request({
    url: url + '/core/tenant/add',
    method: 'post',
    meta: {
      isSerialize: true
    },
    data: data
  })
}
export const update = (id, data) => request({
  url: url + '/core/tenant/update',
  method: 'put',
  meta: {
    isSerialize: true
  },
  data: data
});
/**
 * 全局配置文件
 */
export default {
  title: "JPower管理平台",
  logo: "JP",
  key: 'jp',//配置主键,目前用于存储
  indexTitle: 'JPower管理平台',
  lockPage: '/lock',
  //客户端标识
  clientCode: 'admin',
  //客户端密钥
  clientSecret: 'SCewmm',
  //租户Code HEADER
  tenantCodeHeader: 'Tenant-Code',
  //菜单Code HEADER
  menuCodeHeader: 'Menu-Code',
  //http的status默认放行不才用统一处理的,
  statusWhiteList: [],
  //配置首页不可关闭
  isFirstPage: false,
  //超级用户角色ID；用户超级用户ID也用这个
  rootRoleId: "1",
  //匿名用户角色ID；用户匿名用户ID也用这个
  anonymousRoleId: "2",
  //是否开启多租户模式
  isTenant: true,
  //是否开启验证码登录
  isCaptcha: true,
  fistPage: {
    label: "首页",
    value: "/wel/index",
    params: {},
    query: {},
    meta: {
      i18n: 'dashboard'
    },
    group: [],
    code: '',
    close: false
  },
  //配置菜单的属性
  menu: {
    iconDefault: 'icon-caidan',
    props: {
      label: 'functionName',
      path: 'url',
      icon: 'icon',
      children: 'children',
      hasChildren: 'hasChildren',
      code: 'code',
    }
  }
}
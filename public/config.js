// 主系统地址
//正式环境 
window.home_url = '/api'
const home_url = window.home_url;
window.urllist = {
  // 系统管理
  system: home_url + '/jpower-system',
  // 用户管理
  user: home_url + '/jpower-user',
  //统一授权
  auth: home_url + '/jpower-auth',
  //文件管理
  file: home_url + '/jpower-file',
  //日志管理
  log: home_url + '/jpower-log',

  //字典请求地址
  dictUrl: home_url + '/jpower-system/core/dict/getDictListByType?dictTypeCode=',
  //租户选择项列表
  tenantSelectors: home_url + '/jpower-system/core/tenant/selectors',
  //文件上传地址
  update: home_url + '/jpower-file/core/file/upload',
  //文件下载地址
  download: home_url + '/jpower-file/core/file/download?base='
}
window.$KEY = (function () {
  return {
    AUTH: 'jpower-auth'
  }
})();